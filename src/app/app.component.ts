import { Component } from '@angular/core';
import { ArtistServiceService } from 'src/app/artist-service.service';
import { ArtistDTO } from './models/ArtistDTO';
import { AlbumDTO } from './models/AlbumDTO';
import { StatisticsDTO } from './models/StatisticsDTO';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'manage-artist-ui';
  ArtistsDTO :ArtistDTO[ ]=[ ] ;

  AlbumsDTO : AlbumDTO[ ]=[ ] ;
 StatisticsDTO :StatisticsDTO [ ]=[ ] ;

 ngOnInit(){
  this.getAllArtist();
}
  
  constructor(private artistServiceService: ArtistServiceService) {
  }
  
  public getStatistics(){
    this.artistServiceService.getArtistStatistics().subscribe((s:ArtistDTO[ ]) => {
      this.ArtistsDTO=s
      console.log(JSON.stringify(s))
    });
  }

  public getStatist(){
    this.artistServiceService.getArtistStatistics().subscribe((s:StatisticsDTO[ ]) => {
      this.StatisticsDTO=s
      console.log(JSON.stringify(s))
    });
  }

  public getArtistAlbums(){
    this.artistServiceService.getArtistWithAlbums().subscribe((s:AlbumDTO[ ]) => {
      this.AlbumsDTO=s
      console.log(JSON.stringify(s))
    });
  }

  public getAllArtist(){
    this.artistServiceService.getAllArtist().subscribe((s:ArtistDTO[ ]) => {
      this.ArtistsDTO=s
      console.log(JSON.stringify(s))
    });
  }

}