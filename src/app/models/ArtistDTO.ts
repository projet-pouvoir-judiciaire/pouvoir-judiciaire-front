import { AlbumDTO } from "./AlbumDTO";

export class ArtistDTO {
    artistId: number;
	name: String ;
    albums : AlbumDTO[ ] =[ ] ;
}
