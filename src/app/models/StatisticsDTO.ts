import { map } from "rxjs";
import { ArtistDTO } from "./ArtistDTO";

export class StatisticsDTO {
    artistsDTO: ArtistDTO;
    yearTrackCountSold :Map<number,number>;
    bestYearCount:Number;

    
}
 