import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from './environment';
import { ArtistDTO } from './models/ArtistDTO';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ArtistServiceService {

  URL1 = environment.API_URL1; // endpoint URL
  URL2 = environment.API_URL2; // endpoint URL
  URL3= environment.API_URL3; // endpoint URL

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }


  constructor( private httpClient: HttpClient ) { }

  getArtistStatistics():Observable<any> {
  
    return this.httpClient.get(`${this.URL1}`);
  }  


  getArtistWithAlbums():Observable<any> {
  
    return this.httpClient.get(`${this.URL2}`);
  }  

  getAllArtist():Observable<any> {
  
    return this.httpClient.get(`${this.URL3}`);
  }  

}